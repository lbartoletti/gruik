package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/lrstanley/girc"
	"github.com/mmcdole/gofeed"
	"github.com/spf13/viper"
)

type News struct {
	Origin string `json:"origin"`
	Title  string `json:"title"`
	Link   string `json:"link"`
	Date   string `json:"date"`
	Hash   string `json:"hash"`
}

var newsList []News

// Create a hash out of news title and link
func mkHash(s1, s2 string) string {
	s := s1 + s2
	h := sha256.Sum256([]byte(s))
	hash := hex.EncodeToString(h[:])
	return hash[:8]
}

// Checks if news exists by searching its hash
func newsExists(news News) bool {
	for _, n := range newsList {
		if n.Hash == news.Hash {
			return true
		}
	}
	return false
}

// Retrieve a news by its hash
func getNewsByHash(hash string) News {
	hash = strings.ReplaceAll(hash, "#", "")
	for _, n := range newsList {
		if n.Hash == hash {
			return n
		}
	}
	return News{}
}

// Retrieve news from a certain origin
func getNewsByOrigin(origin string) []News {
	resNews := []News{}

	for _, n := range newsList {
		if n.Origin == origin {
			resNews = append(resNews, n)
		}
	}
	return resNews
}

func fmtNews(news News) string {
	return fmt.Sprintf("[{%s}%s{r}] {%s}%s{r} {%s}%s{r} {%s}#%s{r}",
		viper.GetString("irc.colors.origin"),
		news.Origin,
		viper.GetString("irc.colors.title"),
		news.Title,
		viper.GetString("irc.colors.link"),
		news.Link,
		viper.GetString("irc.colors.hash"),
		news.Hash)
}

// Fetch and post news from RSS feeds
func newsFetch(client *girc.Client, channel string) {

	newsList = make([]News, 0)

	feedFile := channel + "-feed.json"
	// load saved news
	f, err := os.OpenFile(feedFile, os.O_CREATE|os.O_RDWR, 0o644)
	if err != nil {
		log.Fatalf("can't open %s: %v", feedFile, err)
	}
	defer f.Close()
	decoder := json.NewDecoder(f)
	if err := decoder.Decode(&newsList); err != nil {
		log.Println("could not load news list, empty?")
	}

	for {
		if client.IsConnected() && len(client.ChannelList()) != 0 {
			break
		}
		log.Printf("%v, not connected, waiting...\n", client.ChannelList())

		time.Sleep(viper.GetDuration("feeds.frequency"))
	}

	for {
		for _, feedURL := range viper.GetStringSlice("feeds.urls") {
			log.Printf("fetching %s...\n", feedURL)
			fp := gofeed.NewParser()
			feed, err := fp.ParseURL(feedURL)
			if err != nil {
				log.Printf("Failed to fetch feed '%s': %s", feedURL, err)
				continue
			}

			i := 0 // number of posted news
			for _, item := range feed.Items {
				news := News{
					Origin: feed.Title,
					Title:  item.Title,
					Link:   item.Link,
					Date:   item.PublishedParsed.String(),
					Hash:   mkHash(item.Title, item.Link),
				}
				// Check if item was already posted
				if newsExists(news) {
					log.Printf("already posted %s (%s)\n", item.Title, news.Hash)
					continue
				}
				// don't paste news older than feeds.maxage
				if time.Since(*item.PublishedParsed) > viper.GetDuration("feeds.maxage") {
					log.Printf("news too old (%s)\n", item.Published)
					continue
				}
				i++
				if i > viper.GetInt("feeds.maxnews") {
					log.Println("too many lines to post")
					break
				}

				client.Cmd.Message(channel, girc.Fmt(fmtNews(news)))

				time.Sleep(viper.GetDuration("irc.delay"))

				// Mark item as posted
				if len(newsList) < viper.GetInt("feeds.ringsize") {
					newsList = append(newsList, news)
				} else {
					newsList = append(newsList[1:], news)
				}
			}
		}
		// save news list to disk to avoid repost when restarting
		if err := f.Truncate(0); err != nil {
			log.Fatal(err)
		}
		if _, err = f.Seek(0, 0); err != nil {
			log.Fatal(err)
		}
		encoder := json.NewEncoder(f)
		if err = encoder.Encode(newsList); err != nil {
			client.Cmd.Message(channel, "could write newsList")
		}
		time.Sleep(viper.GetDuration("feeds.frequency"))
	}
}

func confDefault() {
	kv := map[string]interface{}{
		"irc.server":        "irc.libera.chat",
		"irc.nick":          "gruik",
		"irc.channel":       "goaste",
		"irc.xchannels":     []string{"goaste2"},
		"irc.debug":         false,
		"irc.port":          6667,
		"irc.delay":         "2s",
		"irc.colors.origin": "pink",
		"irc.colors.title":  "bold",
		"irc.colors.link":   "lightblue",
		"irc.colors.hash":   "lightgrey",
		"feeds.urls":        []string{},
		"feeds.maxnews":     10,
		"feeds.maxage":      "1h",
		"feeds.frequency":   "10m",
		"feeds.ringsize":    100,
	}

	for k, v := range kv {
		if !viper.IsSet(k) {
			viper.Set(k, v)
		}
	}
}

func isOp(nick string) bool {
	for _, op := range viper.GetStringSlice("irc.ops") {
		if nick == op {
			return true
		}
	}
	return false
}

// Get the second part of a command
func getParam(s string) string {
	if !strings.Contains(s, " ") {
		return ""
	}
	return s[strings.LastIndex(s, " ")+1:]
}

func main() {
	config := "config"
	if len(os.Args) > 1 {
		config = os.Args[1]
	}

	viper.SetConfigName(config)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.WatchConfig()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read configuration file: %s", err)
	}

	nick := viper.GetString("irc.nick")
	password := viper.GetString("irc.password")
	name := nick
	user := strings.ToLower(nick)
	channel := viper.GetString("irc.channel")
	debug := io.Discard
	if viper.GetBool("irc.debug") {
		debug = os.Stdout
	}

	confDefault() // load defaults for unset parameters

	client := girc.New(girc.Config{
		Server:     viper.GetString("irc.server"),
		Nick:       nick,
		Port:       viper.GetInt("irc.port"),
		Debug:      debug,
		User:       user,
		Name:       name,
		AllowFlood: true,
	})
	if len(password) > 0 {
		client.Config.SASL = &girc.SASLPlain{
			User: user,
			Pass: password,
		}
	}

	client.Handlers.Add(girc.CONNECTED, func(c *girc.Client, e girc.Event) {
		c.Cmd.Join(channel)

		// join secondary channels for xposting
		for _, xchan := range viper.GetStringSlice("irc.xchannels") {
			c.Cmd.Join(xchan)
		}
	})
	client.Handlers.Add(girc.PRIVMSG, func(c *girc.Client, e girc.Event) {
		dest := channel

		if len(e.Params) > 0 && e.Params[0] != channel {
			dest = e.Source.Name
		}
		if isOp(e.Source.Name) && strings.HasPrefix(e.Last(), "!die") {
			c.Close()
		}
		if strings.HasPrefix(e.Last(), "!lsfeeds") {
			for i, f := range viper.GetStringSlice("feeds.urls") {
				n := strconv.Itoa(i + 1)
				c.Cmd.Message(dest, n+". "+f)
				time.Sleep(viper.GetDuration("irc.delay"))
			}
		}
		if isOp(e.Source.Name) && strings.HasPrefix(e.Last(), "!addfeed") {
			s := getParam(e.Last())
			if s == "" {
				return
			}
			ss := append(viper.GetStringSlice("feeds.urls"), s)
			viper.Set("feeds.urls", ss)
			c.Cmd.ReplyTo(e, girc.Fmt("feed {b}{green}added{c}{b}"))
			if err := viper.WriteConfig(); err != nil {
				c.Cmd.ReplyTo(e, girc.Fmt("adding feed {b}{red}failed{c}{b}"))
			}
		}
		if isOp(e.Source.Name) && strings.HasPrefix(e.Last(), "!rmfeed") {
			s := getParam(e.Last())
			if s == "" {
				return
			}
			ss := viper.GetStringSlice("feeds.urls")
			i, err := strconv.Atoi(s)
			if err != nil {
				c.Cmd.ReplyTo(e, "index conversion failed")
				return
			}
			if i < 1 || i > len(ss) {
				c.Cmd.ReplyTo(e, "bad index number")
				return
			}
			ss = append(ss[:i-1], ss[i:]...)
			viper.Set("feeds.urls", ss)
			c.Cmd.ReplyTo(e, girc.Fmt("feed {b}{green}removed{c}{b}"))
			if err := viper.WriteConfig(); err != nil {
				c.Cmd.ReplyTo(e, girc.Fmt("removing feed {b}{red}failed{c}{b}"))
			}
		}
		if strings.HasPrefix(e.Last(), "!xpost") && e.Params[0] == channel {
			s := getParam(e.Last())
			if s == "" {
				return
			}
			for _, xchan := range viper.GetStringSlice("irc.xchannels") {
				if news := getNewsByHash(s); news.Hash != "" {
					post := fmt.Sprintf(" {r}(from %s on %s)", e.Source.Name, channel)
					c.Cmd.Message(xchan, girc.Fmt(fmtNews(news)+post))
				}
			}
		}
		if strings.HasPrefix(e.Last(), "!latest") && e.Params[0] != channel {
			numNews := len(newsList)
			if numNews < 1 {
				c.Cmd.Message(dest, "no news available")
				return
			}
			p := strings.SplitN(e.Last(), " ", 3)
			if len(p) < 2 {
				c.Cmd.Message(dest, "usage: !latest <number> [origin]")
				return
			}
			// n == number of news to show
			n, err := strconv.Atoi(p[1])
			if err != nil {
				c.Cmd.Message(dest, "conversion error")
				return
			}
			showNews := newsList
			// there was a second parameter, specific origin
			if len(p) > 2 {
				showNews = getNewsByOrigin(p[2])
				numNews = len(showNews)
			}
			// user gave a greater number that we have news
			if n > numNews {
				n = numNews
			}
			numNews--
			for i := 0; i < n; i++ {
				fmt.Println(i)
				c.Cmd.Message(dest, girc.Fmt(fmtNews(showNews[numNews-i])))
				time.Sleep(viper.GetDuration("irc.delay"))
			}
		}
	})

	go newsFetch(client, channel)

	for {
		if err := client.Connect(); err != nil {
			log.Printf("Failed to connect to IRC server: %s", err)
			log.Println("reconnecting in 30 seconds...")
			time.Sleep(30 * time.Second)
		} else {
			return
		}
	}
}
